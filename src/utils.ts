
export class Utils {
    public static pickRandom(items: any[]): any {
        return items[Math.floor(Math.random() * items.length)];
    }

    public static weightedPick(items: WeightedItems, exclude?: string): string | undefined {
        let totalWeight = 0;
        const itemLevels = [];
        for (const name in items) {
            if (name === exclude) {
                continue;
            }
            totalWeight += items[name];
            itemLevels.push({ item: name, range: totalWeight });
        }

        const pickWeight = totalWeight * Math.random();
        for (let i = 0, len = itemLevels.length; i < len; i++) {
            if (itemLevels[i].range >= pickWeight) {
                return itemLevels[i].item;
            }
        }
    }
}
