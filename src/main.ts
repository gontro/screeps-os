
// Main imports
import "./creep/creep";
import "./room/room";
import "./structure/structure";

// Configure global logging
import * as _log from "./logger";
global.log = { debug: _log.debug, error: _log.error, info: _log.info, system: _log.system, trace: _log.trace, warning: _log.warning };
log.system(`💫 Booting OS ..`);

// Bootstrap variables
const CLEANUP_INTERVAL = 30;

// Main loop function
const bootstrap = (): void => {
    // Initialize defaults
    if (Memory.logLevel == null) {
        Memory.logLevel = LogLevel.INFO;
    }
    if (Memory.counter == null) {
        Memory.counter = {
            cleanup: 0
        };
    }

    // Cleanup memory
    if (Game.time >= Memory.counter.cleanup) {
        // Clear memory for non-existent spawns
        for (const name in Memory.spawns) {
            if (Game.spawns[name] == null) {
                log.info(`🗑️ Garbage collecting spawn : ${name}`);
                delete Memory.spawns[name];
            }
        }

        // Clear memory for non-existent creeps
        for (const name in Memory.creeps) {
            if (Game.creeps[name] == null) {
                log.info(`🗑️ Garbage collecting creep : ${name}`);
                delete Memory.creeps[name];
            }
        }

        // Clear memory for non-existent flags
        for (const name in Memory.flags) {
            if (Game.flags[name] == null) {
                log.info(`🗑️ Garbage collecting flag : ${name}`);
                delete Memory.flags[name];
            }
        }

        Memory.counter.cleanup = Game.time + CLEANUP_INTERVAL;
    }

    // Process rooms
    for (const name in Game.rooms) {
        const room = Game.rooms[name];

        // Draw room HUD on screen
        room.hud.render();

        // Check count of creeps belonging to this room and completed projects
        if (Game.time >= room.counter.audit) {
            room.audit();
            room.counter.audit = Game.time + Room.AUDIT_INTERVAL;
        }

        // Attempt to spawn new creep
        if (Game.time >= room.counter.spawn) {
            room.spawn();
            room.counter.spawn = Game.time + Room.SPAWN_INTERVAL;
        }

        // Detect controller upgrade/downgrade
        if (room.controller && room.controller.my && room.level !== room.controller.level) {
            room.level = room.controller.level;
            room.manager.planProjects();
        }

        // Animate creeps
        for (let i = 0, len = room.list.creeps.length; i < len; i++) {
            room.list.creeps[i].animate();
        }
    }
};

/**
 * Screeps system expects this "loop" method in main.js to run the
 * application. If we have this line, we can be sure that the globals are
 * bootstrapped properly and the game loop is executed.
 * http://support.screeps.com/hc/en-us/articles/204825672-New-main-loop-architecture
 *
 * @export
 */
export const loop = () => {
    bootstrap();
};
