
type RoomPositionCallback = (pos: RoomPosition, prevPos?: RoomPosition) => boolean;

interface RoomPositionConstructor {
    fromHash(hash: string, roomName: string): RoomPosition;
    fromHashes(hashes: string[], roomName: string): RoomPosition[];
}

interface RoomPosition {
    hash: string;
    terrain: string;
    isOccupied: boolean;
    toVector(to: RoomPosition): IVector;
    toRect(radius: number): IRect;
    toBorder(radius: number, width: number): IBorder;
}
