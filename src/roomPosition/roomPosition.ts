
import {Border} from "./shapes/border";
import {Rect} from "./shapes/rect";
import {Vector} from "./shapes/vector";

RoomPosition.fromHash = (hash: string, roomName: string): RoomPosition => {
    return new RoomPosition(Math.floor(+hash / 50), +hash % 50, roomName);
};

RoomPosition.fromHashes = (hashes: string[], roomName: string): RoomPosition[] => {
    return _.reduce(hashes, (result: RoomPosition[], item: string) => {
        result.push(RoomPosition.fromHash(item, roomName));
        return result;
    }, []);
};

Object.defineProperty(RoomPosition.prototype, "hash", {
    get(): string {
        return `${50 * this.x + this.y}`;
    }
});

Object.defineProperty(RoomPosition.prototype, "terrain", {
    get(): string {
        return Game.map.getTerrainAt(this.x, this.y, this.roomName);
    }
});

Object.defineProperty(RoomPosition.prototype, "isOccupied", {
    get(): number {
        return this.terrain === "wall" ||
            this.lookFor(LOOK_STRUCTURES).length ||
            this.lookFor(LOOK_CONSTRUCTION_SITES).length;
    }
});

RoomPosition.prototype.toVector = function(to: RoomPosition): Vector {
    return new Vector(this, to);
};

RoomPosition.prototype.toRect = function(radius: number): Rect {
    return new Rect(
        new RoomPosition(this.x - radius, this.y - radius, this.roomName),
        new RoomPosition(this.x + radius, this.y + radius, this.roomName)
    );
};

RoomPosition.prototype.toBorder = function(radius: number, width: number): Border {
    return new Border(new Rect(
        new RoomPosition(this.x - radius - width, this.y - radius - width, this.roomName),
        new RoomPosition(this.x + radius - width, this.y + radius - width, this.roomName)
    ), new Rect(
        new RoomPosition(this.x - radius, this.y - radius, this.roomName),
        new RoomPosition(this.x + radius, this.y + radius, this.roomName)
    ));
};
