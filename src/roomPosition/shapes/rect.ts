
// W2N5:1234-348
export class Rect implements IRect {
    public static fromHash(hash: string): Rect {
        const args = hash.split(":");
        const pos = args[1].split("-");
        return new Rect(RoomPosition.fromHash(pos[0], args[0]), RoomPosition.fromHash(pos[1], args[0]));
    }

    public static getBoundedValue(value: number): number {
        if (value > 49) {
            return 49;
        } else if (value < 0) {
            return 0;
        } else {
            return value;
        }
    }

    public topLeft: RoomPosition;
    public bottomRight: RoomPosition;

    get hash(): string {
        return `${this.roomName}:${this.topLeft.hash}-${this.bottomRight.hash}`;
    }

    get top(): number {
        return this.topLeft.y;
    }

    get left(): number {
        return this.topLeft.x;
    }

    get bottom(): number {
        return this.bottomRight.y;
    }

    get right(): number {
        return this.bottomRight.x;
    }

    get roomName(): string {
        return this.topLeft.roomName;
    }

    constructor(topLeft: RoomPosition, bottomRight: RoomPosition) {
        this.topLeft = topLeft;
        this.bottomRight = bottomRight;
    }

    public walk(callback?: RoomPositionCallback, from?: RoomPosition): RoomPosition[] {
        let path: RoomPosition[] = [];
        for (let x = this.left; x <= this.right; x++) {
            for (let y = this.top; y <= this.bottom; y++) {
                path.push(new RoomPosition(x, y, this.roomName));
            }
        }
        if (from) {
            path = _.sortBy(path, (pos: RoomPosition) => from.getRangeTo(pos));
        }

        if (callback) {
            const result: RoomPosition[] = [];
            for (let i = 0, len = path.length; i < len; i++) {
                const pos = new RoomPosition(path[i].x, path[i].y, this.roomName);
                if (i !== 0) {
                    if (callback(pos, new RoomPosition(path[i - 1].x, path[i - 1].y, this.roomName))) {
                        result.push(pos);
                    }
                } else {
                    if (callback(pos)) {
                        result.push(pos);
                    }
                }
            }
            return result;
        } else {
            return path;
        }
    }
}
