
interface IVector {
    hash: string;
    revHash: string;
    dx: number;
    dy: number;
    from: RoomPosition;
    to: RoomPosition;
    roomName: string;
    rect: IRect;
    walk(callback?: RoomPositionCallback, from?: RoomPosition): RoomPosition[];
    walkPath(callback?: RoomPositionCallback, opts?: FindPathOpts): RoomPosition[];
}

interface IRect {
    hash: string;
    topLeft: RoomPosition;
    bottomRight: RoomPosition;
    top: number;
    left: number;
    bottom: number;
    right: number;
    roomName: string;
    walk(callback?: RoomPositionCallback, from?: RoomPosition): RoomPosition[];
}

interface IBorder {
    hash: string;
    innerRect: IRect;
    outerRect: IRect;
    roomName: string;
    walk(callback?: RoomPositionCallback, from?: RoomPosition): RoomPosition[];
}
