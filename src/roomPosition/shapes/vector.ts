
import {Rect} from "./rect";

// W2N5:348-374
export class Vector implements IVector {
    public static fromHash(hash: string): Vector {
        const args = hash.split(":");
        const pos = args[1].split("-");
        return new Vector(RoomPosition.fromHash(pos[0], args[0]), RoomPosition.fromHash(pos[1], args[0]));
    }

    public from: RoomPosition;
    public to: RoomPosition;

    get hash(): string {
        return `${this.roomName}:${this.from.hash}-${this.to.hash}`;
    }

    get revHash(): string {
        return `${this.roomName}:${this.to.hash}-${this.from.hash}`;
    }

    get dx(): number {
        return this.to.x - this.from.x;
    }

    get dy(): number {
        return this.to.y - this.from.y;
    }

    get roomName(): string {
        return this.from.roomName;
    }

    private _rect: Rect;
    get rect(): Rect {
        if (!this._rect) {
            this._rect = new Rect(
                new RoomPosition(this.from.x < this.to.x ? this.from.x : this.to.x, this.from.y < this.to.y ? this.from.y : this.to.y, this.roomName),
                new RoomPosition(this.from.x > this.to.x ? this.from.x : this.to.x, this.from.y > this.to.y ? this.from.y : this.to.y, this.roomName)
            );
        }
        return this._rect;
    }

    constructor(from: RoomPosition, to: RoomPosition) {
        this.from = from;
        this.to = to;
    }

    public walk(callback?: RoomPositionCallback, from?: RoomPosition): RoomPosition[] {
        let path: RoomPosition[] = [];
        for (let x = this.rect.left; x <= this.rect.right; x++) {
            for (let y = this.rect.top; y <= this.rect.bottom; y++) {
                path.push(new RoomPosition(x, y, this.roomName));
            }
        }
        if (from) {
            path = _.sortBy(path, (pos: RoomPosition) => from.getRangeTo(pos));
        }

        if (callback) {
            const result: RoomPosition[] = [];
            for (let i = 0, len = path.length; i < len; i++) {
                const pos = new RoomPosition(path[i].x, path[i].y, this.roomName);
                if (i !== 0) {
                    if (callback(pos, new RoomPosition(path[i - 1].x, path[i - 1].y, this.roomName))) {
                        result.push(pos);
                    }
                } else {
                    if (callback(pos)) {
                        result.push(pos);
                    }
                }
            }
            return result;
        } else {
            return path;
        }
    }

    public walkPath(callback?: RoomPositionCallback, opts?: FindPathOpts): RoomPosition[] {
        const path: RoomPosition[] = [this.from];
        const steps = this.from.findPathTo(this.to, opts);
        for (let i = 0, len = steps.length; i < len; i++) {
            path.push(new RoomPosition(steps[i].x, steps[i].y, this.roomName));
        }

        if (callback) {
            const result: RoomPosition[] = [];
            for (let i = 0, len = path.length; i < len; i++) {
                const pos = new RoomPosition(path[i].x, path[i].y, this.roomName);
                if (i !== 0) {
                    if (callback(pos, new RoomPosition(path[i - 1].x, path[i - 1].y, this.roomName))) {
                        result.push(pos);
                    }
                } else {
                    if (callback(pos)) {
                        result.push(pos);
                    }
                }
            }
            return result;
        } else {
            return path;
        }
    }
}
