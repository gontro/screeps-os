
import {Rect} from "./rect";

// W2N5:3423-438,3490-239
export class Border implements IBorder {
    public static fromHash(hash: string): Border {
        const args = hash.split(":");
        const ranges = args[1].split(",");
        const innerPos = ranges[0].split("-");
        const outerPos = ranges[1].split("-");
        return new Border(
            new Rect(RoomPosition.fromHash(innerPos[0], args[0]), RoomPosition.fromHash(innerPos[1], args[0])),
            new Rect(RoomPosition.fromHash(outerPos[0], args[0]), RoomPosition.fromHash(outerPos[1], args[0]))
        );
    }

    public innerRect: Rect;
    public outerRect: Rect;

    get hash(): string {
        return `${this.roomName}:${this.innerRect.topLeft.hash}-${this.innerRect.bottomRight.hash},${this.outerRect.topLeft.hash}-${this.outerRect.bottomRight.hash}`;
    }

    get roomName(): string {
        return this.innerRect.roomName;
    }

    constructor(inner: Rect, outer: Rect) {
        this.innerRect = inner;
        this.outerRect = outer;
    }

    public walk(callback?: RoomPositionCallback, from?: RoomPosition): RoomPosition[] {
        let path: RoomPosition[] = [];
        for (let x = this.outerRect.left; x <= this.outerRect.right; x++) {
            for (let y = this.outerRect.top; y <= this.outerRect.bottom; y++) {
                if ((x < this.innerRect.left || x > this.innerRect.right) &&
                    (y < this.innerRect.top || y > this.innerRect.bottom)) {
                    path.push(new RoomPosition(x, y, this.roomName));
                }
            }
        }
        if (from) {
            path = _.sortBy(path, (pos: RoomPosition) => from.getRangeTo(pos));
        }

        if (callback) {
            const result: RoomPosition[] = [];
            for (let i = 0, len = path.length; i < len; i++) {
                const pos = new RoomPosition(path[i].x, path[i].y, this.roomName);
                if (i !== 0) {
                    if (callback(pos, new RoomPosition(path[i - 1].x, path[i - 1].y, this.roomName))) {
                        result.push(pos);
                    }
                } else {
                    if (callback(pos)) {
                        result.push(pos);
                    }
                }
            }
            return result;
        } else {
            return path;
        }
    }
}
