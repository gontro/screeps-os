declare namespace NodeJS {
    interface Global {
        log: Log;
    }
}

interface Log {
    error(message: string): void;
    warning(message: string): void;
    system(message: string): void;
    info(message: string): void;
    debug(message: string): void;
    trace(message: string): void;
}

declare const log: Log;

interface Memory {
    logLevel: number;
    counter: any;
    cache: any;
}

interface Game {
    cache: any;
}

interface WeightedItems {
    [item: string]: number;
}
