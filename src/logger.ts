
declare global {
    const enum LogLevel {
        ERROR   = 0,
        WARNING = 1,
        SYSTEM  = 2,
        INFO    = 3,
        DEBUG   = 4,
        TRACE   = 5
    }

    const enum LogColor {
        DEFAULT = "#b2b2b2",
        MUTED   = "#828282",
        ERROR   = "#e06c75",
        WARNING = "#e5bd6d",
        SYSTEM  = "#61afef",
        INFO    = "#98c376",
        DEBUG   = "#c678dd",
        TRACE   = "#828282"
    }

    const enum LogStatus {
        ERROR   = "🔥 ERROR  ",
        WARNING = "🚨 WARNING",
        SYSTEM  = "🌐 SYSTEM ",
        INFO    = "💭 INFO   ",
        DEBUG   = "🔧 DEBUG  ",
        TRACE   = "🔦 TRACE  "
    }
}

function print(message: string, logLevel: LogLevel) {
    const out: string[] = [];
    switch (logLevel) {
        case LogLevel.ERROR:
            out.push(colorText(LogStatus.ERROR, LogColor.ERROR));
            break;
        case LogLevel.WARNING:
            out.push(colorText(LogStatus.WARNING, LogColor.WARNING));
            break;
        case LogLevel.SYSTEM:
            out.push(colorText(LogStatus.SYSTEM, LogColor.SYSTEM));
            break;
        case LogLevel.INFO:
            out.push(colorText(LogStatus.INFO, LogColor.INFO));
            break;
        case LogLevel.DEBUG:
            out.push(colorText(LogStatus.DEBUG, LogColor.DEBUG));
            break;
        case LogLevel.TRACE:
            out.push(colorText(LogStatus.TRACE, LogColor.TRACE));
            break;
        default:
            break;
    }
    out.push(colorText(` - ${Game.time.toString()}`, LogColor.MUTED));
    out.push(colorText(` | ${message}`, LogColor.DEFAULT));
    console.log(out.join(""));
}

export function error(message: string): void {
    if (Memory.logLevel >= LogLevel.ERROR) {
        print(message, LogLevel.ERROR);
    }
}

export function warning(message: string): void {
    if (Memory.logLevel >= LogLevel.WARNING) {
        print(message, LogLevel.WARNING);
    }
}

export function system(message: string): void {
    if (Memory.logLevel >= LogLevel.SYSTEM) {
        print(message, LogLevel.SYSTEM);
    }
}

export function info(message: string): void {
    if (Memory.logLevel >= LogLevel.INFO) {
        print(message, LogLevel.INFO);
    }
}

export function debug(message: string): void {
    if (Memory.logLevel >= LogLevel.DEBUG) {
        print(message, LogLevel.DEBUG);
    }
}

export function trace(message: string): void {
    if (Memory.logLevel >= LogLevel.TRACE) {
        print(message, LogLevel.TRACE);
    }
}

function colorText(message: string, color: string): string {
    return `<font color='${color}'>${message}</font>`;
}
