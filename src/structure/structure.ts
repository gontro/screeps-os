
import "./structureSpawn";

Object.defineProperty(Structure.prototype, "cache", {
    get(): any {
        if (!Game.cache) {
            Game.cache = {};
        }
        if (!Game.cache.structures) {
            Game.cache.structures = {};
        }
        if (!Game.cache.structures.hasOwnProperty(this.id)) {
            Game.cache.structures[this.id] = {};
        }
        return Game.cache.structures[this.id];
    }
});
