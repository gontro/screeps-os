
interface StructureSpawn {
    cache: any;
    manager: ISpawnManager;
    spawn(): number;
}

interface ISpawnManager {
    spawn: Spawn;
    extensions: RoomPosition[];
    streets: RoomPosition[][];
}
