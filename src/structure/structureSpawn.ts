
import {SpawnManager} from "./structureSpawnManager";

Object.defineProperty(StructureSpawn.prototype, "manager", {
    get(): SpawnManager {
        if (!this.cache.manager) {
            this.cache.manager = new SpawnManager(this);
        }
        return this.cache.manager;
    }
});

StructureSpawn.prototype.spawn = function(): number {
    const room = this.room;

    if (!room.controller || !room.controller.my) {
        return ERR_NOT_OWNER;
    }

    // Find appropriate specimen to spawn
    for (const creepRole in Creep.BODY_EVOLUTION) {
        // Check that headcount for this creep role hasn't been reached
        room.recountCreeps(creepRole as CreepRole);
        room.hasReachedHeadcount(creepRole as CreepRole);

        // Get best possible specimens that can spawn with the energy available
        const specimens = _.filter(Creep.BODY_EVOLUTION[creepRole], (body: string[]) => {
            return Creep.getBodyCost(body) <= room.energyAvailable;
        });

        // Determine if we are going to get the best spawn possible or can spawn at all
        if (specimens.length === 0) {
            continue;
        }

        // Attempt spawning with best possible specimen
        const name = Creep.getRandomName();
        const specimen = specimens[specimens.length - 1];
        const status = this.spawnCreep(specimen, name, {
            memory: {
                home: room.name,
                role: creepRole
            }
        });
        if (status === OK) {
            log.info(`🌀 [${this.name}] Spawning ${creepRole} creep ${name} ..`);
            return status;
        } else {
            log.warning(`🌀 [${this.name}] Failed spawning ${creepRole} creep ${name} [${status}] ..`);
        }
    }

    return ERR_NOT_FOUND;
};
