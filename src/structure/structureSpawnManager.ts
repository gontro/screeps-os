
import {RoomManager} from "../room/roomManager";

export class SpawnManager implements ISpawnManager {
    public spawn: Spawn;

    private _extensions: RoomPosition[];
    get extensions(): RoomPosition[] {
        if (!this._extensions) {
            const src = this.spawn.pos;
            this._extensions = src.toRect(RoomManager.SUBURB_WIDTH).walk((pos: RoomPosition) => {
                const vector = src.toVector(pos);
                const isZero = vector.dx === 0 || vector.dy === 0;
                const isCheckerPattern = (vector.dx % 2 === 0 && vector.dy % 2 !== 0) ||
                    (vector.dx % 2 !== 0 && vector.dy % 2 === 0);
                return !isZero && !pos.isOccupied && isCheckerPattern;
            });
            this._extensions = _.sortBy(this._extensions, (pos: RoomPosition) => src.getRangeTo(pos));
        }

        return this._extensions;
    }

    private _streets: RoomPosition[][];
    get streets(): RoomPosition[][] {
        if (!this._streets) {
            this._streets = [];
            const src = this.spawn.pos;
            // West St
            this._streets.push(_.sortBy(src.toRect(RoomManager.SUBURB_WIDTH).walk((pos: RoomPosition) => {
                const delta = src.toVector(pos);
                return delta.dx < -1 && delta.dy === 0 && !pos.isOccupied;
            }), (pos: RoomPosition) => src.getRangeTo(pos)));
            // East St
            this._streets.push(_.sortBy(src.toRect(RoomManager.SUBURB_WIDTH).walk((pos: RoomPosition) => {
                const delta = src.toVector(pos);
                return delta.dx > 1 && delta.dy === 0 && !pos.isOccupied;
            }), (pos: RoomPosition) => src.getRangeTo(pos)));
            // North St
            this._streets.push(_.sortBy(src.toRect(RoomManager.SUBURB_WIDTH).walk((pos: RoomPosition) => {
                const delta = src.toVector(pos);
                return delta.dy < -1 && delta.dx === 0 && !pos.isOccupied;
            }), (pos: RoomPosition) => src.getRangeTo(pos)));
            // South St
            this._streets.push(_.sortBy(src.toRect(RoomManager.SUBURB_WIDTH).walk((pos: RoomPosition) => {
                const delta = src.toVector(pos);
                return delta.dy > 1 && delta.dx === 0 && !pos.isOccupied;
            }), (pos: RoomPosition) => src.getRangeTo(pos)));
        }

        return this._streets;

    }

    constructor(spawn: Spawn) {
        this.spawn = spawn;
    }
}
