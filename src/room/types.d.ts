
interface ArtifactList {
    [hash: string]: IVector | RoomPosition;
}

interface RoomConstructor {
    AUDIT_INTERVAL: number;
    SPAWN_INTERVAL: number;
    HEADCOUNT_EVOLUTION: RoomHeadcountEvolution;
}

interface Room {
    cache: any;
    counter: any;
    role: RoomRole;
    level: number;
    headcount: RoomHeadcount;
    hud: IRoomHud;
    list: IRoomList;
    manager: IRoomManager;
    hasReachedHeadcount(role?: CreepRole): boolean;
    recountCreeps(role?: CreepRole): void;
    audit(): void;
    spawn(): number;
}

interface RoomLeveledPosition {
    [level: number]: RoomPosition[];
}

interface RoomHeadcountEvolution {
    [role: string]: RoomHeadcount[];
}

interface RoomHeadcount {
    [creepRole: string]: number;
}

interface RoomWorkloadEvolution {
    [role: string]: RoomWorkload[];
}

interface RoomWorkload {
    [creepRole: string]: WeightedItems;
}

interface IRoomHud {
    room: Room;
    artifacts: ArtifactList;
    render(): void;
}

interface IRoomList {
    room: Room;
    flags: Flag[];
    creeps: Creep[];
    hostiles: Creep[];
    sources: Source[];
    spawns: Spawn[];
    idleSpawns: Spawn[];
    extensions: Extension[];
    unpowered: Spawn[] | Extension[];
    structures: Structure[];
    hostileStructures: Structure[];
    constructionSites: ConstructionSite[];
}

interface IRoomManager {
    room: Room;
    workload: RoomWorkload;
    projects: IProject[];
    clearProjects(): void;
    addProject(structureType: string, sites: RoomPosition[]): boolean;
    paveSidewalk(pos: RoomPosition): boolean;
    paveRoad(from: RoomPosition, to: RoomPosition): boolean;
    paveRoads(from: RoomPosition[], to: RoomPosition[]): void;
    planStreets(level: number): void;
    planExtensions(level: number): void;
    planRoads(level: number): void;
    planProjects(): void;
    startProject(): boolean;
}
