
export class RoomList implements IRoomList {
    public room: Room;

    private _flags: Flag[];
    get flags(): Flag[] {
        if (!this._flags) {
            this._flags = this.room.find(FIND_FLAGS);
        }
        return this._flags;
    }

    private _creeps: Creep[];
    get creeps(): Creep[] {
        if (!this._creeps) {
            this._creeps = this.room.find(FIND_MY_CREEPS);
        }
        return this._creeps;
    }

    private _hostiles: Creep[];
    get hostiles(): Creep[] {
        if (!this._hostiles) {
            this._hostiles = this.room.find(FIND_HOSTILE_CREEPS);
        }
        return this._hostiles;
    }

    private _sources: Source[];
    get sources(): Source[] {
        if (!this._sources) {
            this._sources = this.room.find(FIND_SOURCES);
        }
        return this._sources;
    }

    private _spawns: Spawn[];
    get spawns(): Spawn[] {
        if (!this._spawns) {
            this._spawns = this.room.find(FIND_MY_SPAWNS);
        }
        return this._spawns;
    }

    get idleSpawns(): Spawn[] {
        return _.filter(this.spawns, (spawn: Spawn) => {
            return spawn.spawning == null;
        });
    }

    private _extensions: Extension[];
    get extensions(): Extension[] {
        if (!this._extensions) {
            this._extensions = _.filter(this.structures, (structure: Structure) => {
                return structure.structureType === STRUCTURE_EXTENSION;
            }) as Extension[];
        }
        return this._extensions;
    }

    private _unpowered: Spawn[] | Extension[];
    get unpowered(): Spawn[] | Extension[] {
        if (!this._unpowered) {
            let targets: Spawn | Extension[] = [];
            targets = targets.concat(this.spawns, this.extensions);
            this._unpowered = _.filter(targets, (structure: Extension | Spawn) => {
                return structure.energy < structure.energyCapacity;
            });
        }
        return this._unpowered;
    }

    private _structures: Structure[];
    get structures(): Structure[] {
        if (!this._structures) {
            this._structures = this.room.find(FIND_MY_STRUCTURES);
        }
        return this._structures;
    }

    private _hostileStructures: Structure[];
    get hostileStructures(): Structure[] {
        if (!this._hostileStructures) {
            this._hostileStructures = this.room.find(FIND_HOSTILE_STRUCTURES, {
                filter: (structure: Structure) => structure.hits
            });
        }
        return this._hostileStructures;
    }

    private _constructionSites: ConstructionSite[];
    get constructionSites(): ConstructionSite[] {
        if (!this._constructionSites) {
            this._constructionSites = this.room.find(FIND_MY_CONSTRUCTION_SITES);
        }
        return this._constructionSites;
    }

    constructor(room: Room) {
        this.room = room;
    }
}
