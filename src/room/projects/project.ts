
declare global {
    const enum ProjectStatus {
        PENDING     = "_",
        STARTED     = "@",
        COMPLETE    = "^"
    }
}

// W2N5:extension:32:^
// W2N5:rampart:324,342,4328:^
// W2N5:road:324,323,34,209:@ --> if 4 positions and spacing is that of border match visual back to start
// W2N5:road:324,323,34,209,384,2384,54:_ --> complete road path
// W2N5:wall:324,323,334,234:_
export class Project implements IProject {
    public static fromHash(hash: string): Project {
        const argTokens = hash.split(":");
        return new Project(argTokens[0], argTokens[1],
            RoomPosition.fromHashes(argTokens[2].split(","), argTokens[0]),
            argTokens[3] as ProjectStatus);
    }

    get hash(): string {
        return `${this.roomName}:${this.structureType}:${this.siteHashes}:${this.status}`;
    }

    get siteHashes(): string {
        return _.reduce(this.sites, (result: any[], item: RoomPosition) => {
            result.push(item.hash);
            return result;
        }, []).join(",");
    }

    public roomName: string;
    public structureType: string;
    public sites: RoomPosition[];
    public status: ProjectStatus;

    constructor(roomName: string, structureType: string, sites: RoomPosition[], status?: ProjectStatus) {
        this.roomName = roomName;
        this.structureType = structureType;
        this.sites = sites;
        this.status = status ? status : ProjectStatus.PENDING;
    }

    public walk(callback?: RoomPositionCallback): RoomPosition[] {
        if (callback) {
            const result: RoomPosition[] = [];
            for (let i = 0, len = this.sites.length; i < len; i++) {
                const pos = new RoomPosition(this.sites[i].x, this.sites[i].y, this.roomName);
                if (i !== 0) {
                    if (callback(pos, new RoomPosition(this.sites[i - 1].x, this.sites[i - 1].y, this.roomName))) {
                        result.push(pos);
                    }
                } else {
                    if (callback(pos)) {
                        result.push(pos);
                    }
                }
            }
            return result;
        } else {
            return this.sites;
        }
    }
}
