
interface IProject {
    hash: string;
    siteHashes: string;
    roomName: string;
    structureType: string;
    sites: RoomPosition[];
    status: ProjectStatus;
    walk(callback?: RoomPositionCallback): RoomPosition[];
}
