
import {Vector} from "../roomPosition/shapes/vector";
import {Project} from "./projects/project";

export class RoomManager implements IRoomManager {
    public static SUBURB_WIDTH: number = 5;

    public static EXTENSION_LEVEL_MAP: number[] = [0, 0, 5, 10, 20, 30, 40, 50, 60];

    public static WORKLOAD_EVOLUTION: RoomWorkloadEvolution = {
        colony: [
            {
                worker: { harvest: 1, build: 0.5, upgrade: 2 }
            },
            {
                worker: { harvest: 1, build: 0.5, upgrade: 2 }
            },
            {
                worker: { harvest: 1, build: 0.5, upgrade: 2 }
            },
            {
                worker: { harvest: 1, build: 0.5, upgrade: 2 }
            },
            {
                worker: { harvest: 1, build: 0.5, upgrade: 2 }
            },
            {
                worker: { harvest: 1, build: 0.5, upgrade: 2 }
            },
            {
                worker: { harvest: 1, build: 0.5, upgrade: 2 }
            },
            {
                worker: { harvest: 1, build: 0.5, upgrade: 2 }
            }
        ]
    };

    public static roadCostCallback(roomName: string, costMatrix: CostMatrix): boolean | CostMatrix {
        const room = Game.rooms[roomName];
        for (let i = 0, len = room.manager.projects.length; i < len; i++) {
            const project = room.manager.projects[i];
            if (project.structureType !== STRUCTURE_ROAD) {
                for (let j = 0, jLen = project.sites.length; j < jLen; j++) {
                    costMatrix.set(project.sites[j].x, project.sites[j].y, 255);
                }
            }
        }
        for (let i = 0, len = room.list.spawns.length; i < len; i++) {
            const positions = room.list.spawns[i].manager.extensions;
            for (let j = 0, jLen = positions.length; j < jLen; j++) {
                costMatrix.set(positions[j].x, positions[j].y, 255);
            }
        }
        return true;
    }

    public room: Room;

    private _workload: RoomWorkload;
    get workload(): RoomWorkload {
        if (this._workload) {
            return this._workload;
        }

        if (!this.room.controller || !this.room.controller.my) {
            return {};
        }

        const workload = RoomManager.WORKLOAD_EVOLUTION[this.room.role][this.room.level - 1];
        this._workload = _.reduce(workload, (result: any, item: WeightedItems, creepRole: string) => {
            result[creepRole] = {};
            for (const creepJob in item) {
                const creepsByJob = _.filter(this.room.list.creeps, {role: creepRole, job: creepJob});
                const totalRemainingCreeps = this.room.list.creeps.length - creepsByJob.length;
                result[creepRole][creepJob] = totalRemainingCreeps ? item[creepJob] * totalRemainingCreeps / this.room.list.creeps.length : 0;
            }
            return result;
        }, {});

        return this._workload;
    }

    private _projects: Project[];
    get projects(): Project[] {
        if (this.room.memory.projects == null) {
            this.room.memory.projects = [];
        }
        if (this._projects == null) {
            this._projects = _.reduce(this.room.memory.projects, (result: Project[], item: string) => {
                const project = Project.fromHash(item);
                if (project) {
                    result.push(project);
                }
                return result;
            }, []);
        }
        return this._projects;
    }

    private _pavedRoadHashes: string[] = [];

    constructor(room: Room) {
        this.room = room;
    }

    public clearProjects(): void {
        this.room.memory.projects = [];
    }

    public addProject(structureType: string, sites: RoomPosition[]): boolean {
        const project = new Project(this.room.name, structureType, sites);

        if (this.room.memory.projects == null) {
            this.room.memory.projects = [];
        } else if (this.room.memory.projects.indexOf(project.hash) !== -1) {
            return false;
        }

        this.projects.push(project);
        this.room.memory.projects.push(project.hash);
        log.trace(`〽️ [${this.room.name}] Added new project [${project.structureType}:${project.siteHashes}]`);
        return true;
    }

    public paveSidewalk(pos: RoomPosition): boolean {
        const sites: RoomPosition[] = [
            new RoomPosition(pos.x, pos.y - 1, pos.roomName),
            new RoomPosition(pos.x + 1, pos.y, pos.roomName),
            new RoomPosition(pos.x, pos.y + 1, pos.roomName),
            new RoomPosition(pos.x - 1, pos.y, pos.roomName)
        ];

        const validSites: RoomPosition[] = [];
        for (let i = 0, len = sites.length; i < len; i++) {
            if (sites[i].terrain !== "wall") {
                validSites.push(sites[i]);
            }
        }

        return this.addProject(STRUCTURE_ROAD, validSites);
    }

    public paveRoad(from: RoomPosition, to: RoomPosition, usePathing?: boolean): boolean {
        const vector = new Vector(from, to);
        if (vector.from.hash === vector.to.hash ||
            this._pavedRoadHashes.indexOf(vector.hash) !== -1 ||
            this._pavedRoadHashes.indexOf(vector.revHash) !== -1) {
            return false;
        }
        this._pavedRoadHashes.push(vector.hash);

        let result: RoomPosition[];
        if (usePathing) {
            result = vector.walkPath(undefined, {
                costCallback: RoomManager.roadCostCallback, ignoreCreeps: true, ignoreRoads: true
            });
        } else {
            result = vector.walk(undefined, from);
        }
        return this.addProject(STRUCTURE_ROAD, result);
    }

    public paveRoads(from: RoomPosition[], to: RoomPosition[]): void {
        for (let i = 0, iLen = from.length; i < iLen; i++) {
            for (let j = 0, jLen = to.length; j < jLen; j++) {
                this.paveRoad(from[i], to[j], true);
            }
        }
    }

    public planStreets(level: number): void {
        if (level < 1 || level > 1) {
            return;
        }

        for (let i = 0, len = this.room.list.spawns.length; i < len; i++) {
            const spawn = this.room.list.spawns[i];
            log.trace(`🕋 [${this.room.name}] Planning street projects for ${spawn.name} ..`);

            if (level === 1) {
                this.paveRoad(spawn.pos, new RoomPosition(spawn.pos.x, spawn.pos.y - RoomManager.SUBURB_WIDTH, this.room.name));
                this.paveRoad(spawn.pos, new RoomPosition(spawn.pos.x + RoomManager.SUBURB_WIDTH, spawn.pos.y, this.room.name));
                this.paveRoad(spawn.pos, new RoomPosition(spawn.pos.x, spawn.pos.y + RoomManager.SUBURB_WIDTH, this.room.name));
                this.paveRoad(spawn.pos, new RoomPosition(spawn.pos.x - RoomManager.SUBURB_WIDTH, spawn.pos.y, this.room.name));
            }
        }
    }

    public planExtensions(level: number): void {
        if (level < 2 || level > 8) {
            return;
        }

        for (let i = 0, len = this.room.list.spawns.length; i < len; i++) {
            const spawn = this.room.list.spawns[i];
            log.trace(`🕋 [${this.room.name}] Planning level ${level} extension projects for ${spawn.name} ..`);
            const totalExtensionProjects = _.filter(this.projects, {structureType: STRUCTURE_EXTENSION}).length;
            for (let j = totalExtensionProjects; j < RoomManager.EXTENSION_LEVEL_MAP[level]; j++) {
                const pos: RoomPosition = spawn.manager.extensions[j];
                if (spawn.manager.extensions.length > j) {
                    this.addProject(STRUCTURE_EXTENSION, [pos]);
                    this.paveSidewalk(pos);
                }
            }
        }
    }

    public planRoads(level: number): void {
        if (level < 1 || level > 8) {
            return;
        }

        log.trace(`🕋 [${this.room.name}] Planning level ${level} road projects ..`);
        switch (level) {
            case 1:
                // RCL 1 -> controller to sources
                if (this.room.controller) {
                    this.paveRoads([this.room.controller.pos], _.map(this.room.list.sources, "pos"));
                }
                break;
            case 2:
                // RCL 2 -> spawns to controller | sources | containers | ramparts (highway)
                let to: RoomPosition[] = [];
                if (this.room.controller) {
                    to.push(this.room.controller.pos);
                }
                to = to.concat(_.map(this.room.list.sources, "pos"));
                this.paveRoads(_.map(this.room.list.spawns, "pos"), to);
                break;
            case 3:
                // RCL 3 -> spawns to towers
                // this.buildRoads(this.spawns, this.towers);
                break;
            case 4:
                // RCL 4 -> spawns to storages
                // this.buildRoads(this.spawns, this.storages);
                break;
            case 5:
                // RCL 5 -> spawns to links
                // this.buildRoads(this.spawns, this.links);
                break;
            case 6:
                // RCL 6 -> spawns to extractors | labs | terminals
                // this.buildRoads(this.spawns, this.extractors.concat(this.labs, this.terminals));
                break;
            case 7:
                // RCL 7 -> spawns to spawns
                this.paveRoads(_.map(this.room.list.spawns, "pos"), _.map(this.room.list.spawns, "pos"));
                break;
            case 8:
                // RCL 8 -> spawns to observers | powerSpawns | nukers
                // this.buildRoads(this.spawns, this.observers.concat(this.powerSpawns, this.nukers));
                break;
        }
    }

    public planProjects(): void {
        if (this.projects.length) {
            return;
        }

        log.info(`🕋 [${this.room.name}] Planning projects ..`);
        for (let level = 1; level <= 8; level++) {
            this.planStreets(level);
            this.planRoads(level);
            this.planExtensions(level);
        }
    }

    public startProject(): boolean {
        // Return if there are still projects being built
        if (_.filter(this.projects, {status: ProjectStatus.STARTED}).length) {
            return false;
        }

        // Check that there are actually projects to work on
        const projects = _.filter(this.projects, {status: ProjectStatus.PENDING});
        if (!projects.length) {
            return false;
        }

        // Start the first project in the list of pending projects
        // const project = this.deserializeProject(projects[0]);
        // projects[0].status = ProjectStatus.STARTED;
        // if (project.structureType === STRUCTURE_ROAD && project.to) {
        //     const vector = project.pos.getVector(project.to);
        //     vector.filter((pos: RoomPosition) => {
        //         pos.createConstructionSite(STRUCTURE_ROAD);
        //         return true;
        //     }, {
        //         costCallback: RoomManager.roadCostCallback,
        //         ignoreCreeps: true,
        //         ignoreRoads: true
        //     });
        // } else {
        //     project.pos.createConstructionSite(project.structureType);
        // }

        return true;
    }
}
