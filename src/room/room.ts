
import "../roomPosition/roomPosition";

import {Project} from "./projects/project";

import {RoomHUD} from "./roomHud";
import {RoomList} from "./roomList";
import {RoomManager} from "./roomManager";

declare global {
    const enum RoomRole {
        NONE     = "none",
        COLONY   = "colony",
        OUTPOST  = "outpost",
        CORRIDOR = "corridor"
    }
}

Room.SPAWN_INTERVAL = 30;
Room.AUDIT_INTERVAL = 120;

Room.HEADCOUNT_EVOLUTION = {
    colony: [
        { worker: 4 },
        { worker: 5 },
        { worker: 6 },
        { worker: 7 },
        { worker: 7 },
        { worker: 7 },
        { worker: 7 },
        { worker: 7 }
    ]
};

Object.defineProperty(Room.prototype, "cache", {
    get(): any {
        if (!Game.cache) {
            Game.cache = {};
        }
        if (!Game.cache.rooms) {
            Game.cache.rooms = {};
        }
        if (!Game.cache.rooms.hasOwnProperty(this.name)) {
            Game.cache.rooms[this.name] = {};
        }
        return Game.cache.rooms[this.name];
    }
});

Object.defineProperty(Room.prototype, "counter", {
    get(): any {
        if (!this.memory.counter) {
            this.memory.counter = {
                audit: 0,
                spawn: 0
            };
        }
        return this.memory.counter;
    }
});

Object.defineProperty(Room.prototype, "role", {
    get(): RoomRole {
        if (this.memory.role == null) {
            this.memory.role = this.controller && this.controller.my ? RoomRole.COLONY : RoomRole.NONE;
        }
        return this.memory.role;
    }
});

Object.defineProperty(Room.prototype, "level", {
    get(): number {
        if (this.memory.level == null) {
            this.memory.level = this.controller && this.controller.my ? this.controller.level : 0;
        }
        return this.memory.level;
    },
    set(level: number) {
        this.memory.level = level;
    }
});

Object.defineProperty(Room.prototype, "headcount", {
    get(): RoomHeadcount {
        if (this.memory.headcount == null) {
            this.memory.headcount = {};
        }
        return this.memory.headcount;
    }
});

Object.defineProperty(Room.prototype, "hud", {
    get(): RoomHUD {
        if (!this.cache.hud) {
            this.cache.hud = new RoomHUD(this);
        }
        return this.cache.hud;
    }
});

Object.defineProperty(Room.prototype, "list", {
    get(): RoomList {
        if (!this.cache.list) {
            this.cache.list = new RoomList(this);
        }
        return this.cache.list;
    }
});

Object.defineProperty(Room.prototype, "manager", {
    get(): RoomManager {
        if (!this.cache.manager) {
            this.cache.manager = new RoomManager(this);
        }
        return this.cache.manager;
    }
});

Room.prototype.hasReachedHeadcount = function(role?: CreepRole): boolean {
    let result = true;
    const desiredHeadcount = Room.HEADCOUNT_EVOLUTION[this.role][this.level - 1];
    for (const creepRole in desiredHeadcount) {
        if (role && role !== creepRole) {
            continue;
        }
        if (this.headcount.hasOwnProperty(creepRole) && this.headcount[creepRole] < desiredHeadcount[creepRole]) {
            result = false;
        }
    }
    return result;
};

Room.prototype.recountCreeps = function(role?: CreepRole): void {
    log.trace(`🕋 [${this.name}] Recounting ${role ? role : "ALL"} creeps ..`);

    this.memory.headcount = {};
    for (const creepRole in Room.HEADCOUNT_EVOLUTION[this.role][this.level - 1]) {
        if (role && role !== creepRole) {
            continue;
        }
        this.headcount[creepRole] = _.filter(this.list.creeps, (creep: Creep) => {
            return creep.role === creepRole
                && creep.ticksToLive > creep.body.length * CREEP_SPAWN_TIME + Room.SPAWN_INTERVAL;
        }).length;
    }
};

Room.prototype.audit = function(): void {
    log.trace(`🕋 [${this.name}] Performing audit ..`);

    // Update creep headcount for spawning purposes
    this.recountCreeps();

    // Update any projects from STARTED to DONE if applicable
    const projectsStarted: Project[] = _.filter(this.manager.projects, {status: ProjectStatus.STARTED});
    for (let i = 0, len = projectsStarted.length; i < len; i++) {
        const pendingSites = projectsStarted[i].walk((pos: RoomPosition) => {
            return !pos.lookFor(LOOK_CONSTRUCTION_SITES).length &&
                !pos.lookFor(LOOK_STRUCTURES).length;
        });
        if (!pendingSites.length) {
            projectsStarted[i].status = ProjectStatus.COMPLETE;
        } else if (pendingSites.length !== projectsStarted[i].sites.length) {
            projectsStarted[i].status = ProjectStatus.STARTED;
        }
    }

    // Start construction on pre-planned projects
    this.manager.startProject();
};

Room.prototype.spawn = function(): number {
    if (this.role !== RoomRole.COLONY) {
        return ERR_NOT_OWNER;
    }

    if (this.hasReachedHeadcount()) {
        return ERR_FULL;
    }

    if (!this.list.idleSpawns.length) {
        return ERR_BUSY;
    }

    if (this.energyAvailable !== this.energyCapacityAvailable) {
        return ERR_NOT_ENOUGH_ENERGY;
    }

    const spawn = this.list.idleSpawns[0];
    log.trace(`🕋 [${this.name}] Attempting to spawn at ${spawn.name} ..`);
    return spawn.spawn();
};
