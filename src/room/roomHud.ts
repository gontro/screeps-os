
import {Vector} from "../roomPosition/shapes/vector";
import {Project} from "./projects/project";

declare global {
    const enum ArtifactType {
        CIRCLE    = "c",
        LINE      = "l"
    }
}

export class RoomHUD implements IRoomHud {
    public static LINE_STYLES: {[structureType: string]: LineStyle} = {
        road: {color: "#333333", opacity: 0.5, width: 0.2},
        unknown: {color: "#ff0000", opacity: 1, width: 0.2}
    };

    public static CIRCLE_STYLES: {[structureType: string]: CircleStyle} = {
        extension: {stroke: "#cca643", opacity: 0.5, radius: 0.20},
        unknown: {stroke: "#ff0000", opacity: 1, radius: 0.20}
    };

    public room: Room;

    private _artifacts: ArtifactList;
    get artifacts(): ArtifactList {
        if (this._artifacts == null) {
            this._artifacts = {};
        }
        return this._artifacts;
    }

    constructor(room: Room) {
        this.room = room;
    }

    public render(): void {
        if (Memory.logLevel < LogLevel.DEBUG) {
            return;
        }

        // Calculate what to draw and save it to memory
        const projects: Project[] = _.filter(this.room.manager.projects, {status: ProjectStatus.PENDING});
        for (let i = 0, len = projects.length; i < len; i++) {
            const project: Project = projects[i];
            if (project.structureType === STRUCTURE_ROAD ||
                project.structureType === STRUCTURE_WALL ||
                project.structureType === STRUCTURE_RAMPART) {

                project.walk((pos: RoomPosition, prevPos?: RoomPosition) => {
                    if (prevPos) {
                        this.drawLine(project.structureType, new Vector(prevPos, pos));
                    }
                    return true;
                });

                // Link up sidewalks
                if (project.sites.length === 4) {
                    this.drawLine(project.structureType,
                        new Vector(project.sites[project.sites.length - 1], project.sites[0]));
                }
            } else {
                project.walk((pos: RoomPosition) => {
                    this.drawCircle(project.structureType, pos);
                    return true;
                });
            }
        }

        // Render artifacts
        for (const hash in this.artifacts) {
            const args: string[] = hash.split("|");
            if (args[0] === ArtifactType.CIRCLE) {
                const pos: RoomPosition = this.artifacts[hash] as RoomPosition;
                const style: CircleStyle = RoomHUD.CIRCLE_STYLES.hasOwnProperty(args[1]) ?
                    RoomHUD.CIRCLE_STYLES[args[1]] : RoomHUD.CIRCLE_STYLES.unknown;
                this.room.visual.circle(pos, style);
            } else if (args[0] === ArtifactType.LINE) {
                const vector: Vector = this.artifacts[hash] as Vector;
                const style: CircleStyle = RoomHUD.LINE_STYLES.hasOwnProperty(args[1]) ?
                    RoomHUD.LINE_STYLES[args[1]] : RoomHUD.LINE_STYLES.unknown;
                this.room.visual.line(vector.from, vector.to, style);
            }
        }
    }

    private drawCircle(structureType: string, pos: RoomPosition): void {
        this.artifacts[`${ArtifactType.CIRCLE}|${structureType}|${pos.hash}`] = pos;
    }

    private drawLine(structureType: string, vector: Vector): void {
        this.artifacts[`${ArtifactType.LINE}|${structureType}|${vector.hash}`] = vector;
    }
}
