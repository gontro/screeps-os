
import {WorkerRole} from "./roles/workerRole";

import {Utils} from "../utils";

declare global {
    const enum CreepRole {
        WORKER   = "worker"
    }

    const enum CreepJob {
        NONE     = "none",
        HARVEST  = "harvest",
        UPGRADE  = "upgrade",
        BUILD    = "build"
    }

    const enum CreepAction {
        NONE     = "none",
        COLLECT  = "collect",
        DELIVER  = "deliver"
    }
}

Creep.BODY_EVOLUTION = {
    worker: [
        [WORK, WORK, CARRY, MOVE],
        [WORK, WORK, WORK, CARRY, MOVE, MOVE],
        [WORK, WORK, WORK, WORK, CARRY, MOVE, MOVE, MOVE],
        [WORK, WORK, WORK, WORK, CARRY, CARRY, MOVE, MOVE, MOVE, MOVE]
    ]
};

Creep.VISUALS = {
    build: {
        collect: { emoji: "🔌", style: { stroke: "#e5bd6d", opacity: 0.5 } },
        deliver: { emoji: "🔨", style: { stroke: "#98c376", opacity: 0.7 } }
    },
    harvest: {
        collect: { emoji: "🔌", style: { stroke: "#e5bd6d", opacity: 0.5 } },
        deliver: { emoji: "🔋", style: { stroke: "#b26fad", opacity: 0.7 } }
    },
    upgrade: {
        collect: { emoji: "🔌", style: { stroke: "#e5bd6d", opacity: 0.5 } },
        deliver: { emoji: "🚀", style: { stroke: "#61afef", opacity: 0.7 } }
    }
};

Creep.NAMES_MALE = ["Jackson", "Aiden", "Liam", "Lucas", "Noah", "Mason", "Jayden", "Ethan",
    "Jacob", "Jack", "Caden", "Logan", "Benjamin", "Michael", "Caleb", "Ryan", "Alexander", "Elijah", "James",
    "William", "Oliver", "Connor", "Matthew", "Daniel", "Luke", "Brayden", "Jayce", "Henry", "Carter", "Dylan",
    "Gabriel", "Joshua", "Nicholas", "Isaac", "Owen", "Nathan", "Grayson", "Eli", "Landon", "Andrew", "Max",
    "Samuel", "Gavin", "Wyatt", "Christian", "Hunter", "Cameron", "Evan", "Charlie", "David", "Sebastian",
    "Joseph", "Dominic", "Anthony", "Colton", "John", "Tyler", "Zachary", "Thomas", "Julian", "Levi", "Adam",
    "Isaiah", "Alex", "Aaron", "Parker", "Cooper", "Miles", "Chase", "Muhammad", "Christopher", "Blake",
    "Austin", "Jordan", "Leo", "Jonathan", "Adrian", "Colin", "Hudson", "Ian", "Xavier", "Camden", "Tristan",
    "Carson", "Jason", "Nolan", "Riley", "Lincoln", "Brody", "Bentley", "Nathaniel", "Josiah", "Declan", "Jake",
    "Asher", "Jeremiah", "Cole", "Mateo", "Micah", "Elliot"];

Creep.NAMES_FEMALE = ["Sophia", "Emma", "Olivia", "Isabella", "Mia", "Ava", "Lily", "Zoe", "Emily",
    "Chloe", "Layla", "Madison", "Madelyn", "Abigail", "Aubrey", "Charlotte", "Amelia", "Ella", "Kaylee",
    "Avery", "Aaliyah", "Hailey", "Hannah", "Addison", "Riley", "Harper", "Aria", "Arianna", "Mackenzie",
    "Lila", "Evelyn", "Adalyn", "Grace", "Brooklyn", "Ellie", "Anna", "Kaitlyn", "Isabelle", "Sophie",
    "Scarlett", "Natalie", "Leah", "Sarah", "Nora", "Mila", "Elizabeth", "Lillian", "Kylie", "Audrey", "Lucy",
    "Maya", "Annabelle", "Makayla", "Gabriella", "Elena", "Victoria", "Claire", "Savannah", "Peyton", "Maria",
    "Alaina", "Kennedy", "Stella", "Liliana", "Allison", "Samantha", "Keira", "Alyssa", "Reagan", "Molly",
    "Alexandra", "Violet", "Charlie", "Julia", "Sadie", "Ruby", "Eva", "Alice", "Eliana", "Taylor", "Callie",
    "Penelope", "Camilla", "Bailey", "Kaelyn", "Alexis", "Kayla", "Katherine", "Sydney", "Lauren", "Jasmine",
    "London", "Bella", "Adeline", "Caroline", "Vivian", "Juliana", "Gianna", "Skyler", "Jordyn"];

/**
 * Choose a random name from CREEP_NAMES_MALE and CREEP_NAMES_FEMALE.
 * @param {string} prefix
 * @returns {string}
 */
Creep.getRandomName = (prefix?: string): string => {
    let name;
    let isNameTaken = false;
    let tries = 0;

    do {
        const names = Math.random() > .5 ? Creep.NAMES_MALE : Creep.NAMES_FEMALE;
        name = names[Math.floor(Math.random() * names.length)];

        if (tries > 3) {
            name += names[Math.floor(Math.random() * names.length)];
        }

        tries++;
        isNameTaken = Game.creeps[name] !== undefined;
    } while (isNameTaken);

    return prefix ? `${prefix} ${name}` : name;
};

/**
 * Calculate the energy cost of the passed in array of body parts.
 * @param {string[]} body
 * @returns {number}
 */
Creep.getBodyCost = (body: string[]): number => {
    return body.reduce((cost: number, part: string) => {
        return cost + BODYPART_COST[part];
    }, 0);
};

Object.defineProperty(Creep.prototype, "cache", {
    get(): any {
        if (!Game.cache) {
            Game.cache = {};
        }
        if (!Game.cache.creeps) {
            Game.cache.creeps = {};
        }
        if (!Game.cache.creeps.hasOwnProperty(this.name)) {
            Game.cache.creeps[this.name] = {};
        }
        return Game.cache.creeps[this.name];
    }
});

Object.defineProperty(Creep.prototype, "home", {
    get(): Room {
        return Game.rooms[this.memory.home];
    }
});

Object.defineProperty(Creep.prototype, "role", {
    get(): string {
        return this.memory.role;
    }
});

Object.defineProperty(Creep.prototype, "job", {
    get(): string {
        if (!this.memory.job) {
            this.memory.job = CreepJob.NONE;
            this.action = CreepAction.NONE;
            this.target = null;
        }
        return this.memory.job;
    },
    set(job: string): void {
        this.memory.job = job;
        this.action = CreepAction.NONE;
        this.target = null;
    }
});

Object.defineProperty(Creep.prototype, "action", {
    get(): CreepAction {
        if (!this.memory.action) {
            this.memory.action = CreepAction.NONE;
        }
        return this.memory.action;
    },
    set(action: CreepAction): void {
        this.memory.action = action;
    }
});

Object.defineProperty(Creep.prototype, "target", {
    get(): any {
        if (this.memory.targetId) {
            this.cache.target = Game.getObjectById(this.memory.targetId);
        }
        return this.cache.target;
    },
    set(target: any) {
        if (!target) {
            this.memory.targetId = null;
            delete this.cache.target;
        } else {
            this.memory.targetId = target.id;
            this.cache.target = target;
        }
    }
});

Creep.prototype.isFull = function(): boolean {
    return _.sum(this.carry) === this.carryCapacity;
};

Creep.prototype.reportIn = function(message?: string): number {
    log.trace(`🦎 [${this.name}] Triggering (${this.job}.${this.action})->${this.target} ..`);
    const emoji = Creep.VISUALS[this.job][this.action].emoji;
    return this.say(message ? `${emoji} ${message}` : `${emoji} ${this.job}`);
};

Creep.prototype.visualMoveTo = function(target: RoomPosition | { pos: RoomPosition }, opts?: MoveToOpts): number {
    opts = opts ? opts : {};
    if (Memory.logLevel >= LogLevel.INFO) {
        opts.visualizePathStyle = Creep.VISUALS[this.job][this.action].style;
    }
    return this.moveTo(target, opts);
};

Creep.prototype.switchJob = function(includeCurrentJob?: boolean): boolean {
    if (!this.home.controller) {
        return false;
    }

    const job = Utils.weightedPick(this.home.manager.workload[this.role], includeCurrentJob ? "" : this.job) as CreepJob;
    if (job) {
        log.trace(`🦎 [${this.name}] Switching job to ${job.toUpperCase()} ..`);
        this.job = job;
        return true;
    }

    this.job = CreepJob.NONE;
    return false;
};

Creep.prototype.animate = function(): void {
    if (this.fatigue !== 0) {
        return;
    }

    switch (this.role) {
        default:
            return new WorkerRole(this).run();
    }
};
