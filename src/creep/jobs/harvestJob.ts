
import {Utils} from "../../utils";

export class HarvestJob implements CreepJobAI {
    public creep: Creep;

    constructor(creep: Creep) {
        this.creep = creep;
    }

    public run(): void {
        if (this.creep.action === CreepAction.NONE) {
            this.creep.isFull() ? this.trigger(CreepAction.DELIVER) : this.trigger(CreepAction.COLLECT);
        }

        if (this.creep.action === CreepAction.COLLECT) {
            switch (this.creep.harvest(this.creep.target)) {
                case ERR_NOT_IN_RANGE:
                    this.creep.visualMoveTo(this.creep.target);
                    break;
            }

            if (this.creep.isFull()) {
                this.trigger(CreepAction.DELIVER);
            }
        } else if (this.creep.action === CreepAction.DELIVER) {
            switch (this.creep.transfer(this.creep.target, RESOURCE_ENERGY)) {
                case ERR_NOT_IN_RANGE:
                    this.creep.visualMoveTo(this.creep.target);
                    break;
                case ERR_NOT_ENOUGH_RESOURCES:
                    this.trigger(CreepAction.COLLECT);
                    break;
                case ERR_FULL:
                    this.creep.switchJob();
                    break;
            }
        }
    }

    public trigger(action: CreepAction): boolean {
        if (action === CreepAction.COLLECT) {
            this.creep.target = Utils.pickRandom(this.creep.home.list.sources);
        } else if (action === CreepAction.DELIVER) {
            if (this.creep.home.list.unpowered.length) {
                this.creep.target = Utils.pickRandom(this.creep.home.list.unpowered);
            } else {
                return this.creep.switchJob();
            }
        } else {
            return false;
        }

        this.creep.action = action;
        this.creep.reportIn();
        return true;
    }
}
