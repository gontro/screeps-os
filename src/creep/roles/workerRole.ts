
import {BuildJob} from "../jobs/buildJob";
import {HarvestJob} from "../jobs/harvestJob";
import {UpgradeJob} from "../jobs/upgradeJob";

export class WorkerRole implements CreepRoleAI {
    public creep: Creep;

    constructor(creep: Creep) {
        this.creep = creep;
    }

    public run(): void {
        if (this.creep.job === CreepJob.NONE && !this.creep.switchJob(true)) {
            return;
        }

        switch (this.creep.job) {
            case CreepJob.BUILD:
                return new BuildJob(this.creep).run();
            case CreepJob.UPGRADE:
                return new UpgradeJob(this.creep).run();
            default:
                return new HarvestJob(this.creep).run();
        }
    }
}
