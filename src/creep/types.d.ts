
interface CreepConstructor {
    BODY_EVOLUTION: CreepEvolution;
    VISUALS: CreepVisuals;
    NAMES_MALE: string[];
    NAMES_FEMALE: string[];
    getRandomName(prefix?: string): string;
    getBodyCost(body: string[]): number;
}

interface Creep {
    cache: any;
    home: Room;
    role: CreepRole;
    job: CreepJob;
    action: CreepAction;
    target: any;
    isFull(): boolean;
    reportIn(message?: string): number;
    visualMoveTo(target: RoomPosition | { pos: RoomPosition }, opts?: MoveToOpts): number;
    switchJob(includeCurrentJob?: boolean): boolean;
    animate(): void;
}

interface CreepRoleAI {
    creep: Creep;
    run(): void;
}

interface CreepJobAI {
    creep: Creep;
    run(): void;
    trigger(action: CreepAction): boolean;
}

interface CreepVisuals {
    [job: string]: {
        [role: string]: {
            emoji: string,
            style: PolyStyle
        }
    };
}

interface CreepEvolution {
    [role: string]: string[][];
}
